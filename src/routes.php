<?php

Route::any('/ckfinder/connector', 'Emuji\AdminCK\Controllers\CKFinderController@requestAction')
    ->name('ckfinder-connector');

Route::any('/ckfinder/browser', 'Emuji\AdminCK\Controllers\CKFinderController@browserAction')
    ->name('ckfinder-browser');