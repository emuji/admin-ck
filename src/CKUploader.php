<?php

namespace Emuji\AdminCK;

use Emuji\Admin\Form\Field;

class CKUploader extends Field
{
    public static $js = [
        '/vendor/admin-ck/ckfinder/ckfinder.js',
    ];

    protected $view = 'admin-ck::ckuploader';

    public function render()
    {
        $filebrowserUploadUrl = route('ckfinder-connector');
        $this->script = <<<EOT
function selectFileWithCKFinder( el ) {
    CKFinder.config( { connectorPath: '{$filebrowserUploadUrl}' } );
    CKFinder.popup( {
        chooseFiles: true,
        onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
                var url = evt.data.files.first().getUrl();
                var input = $(el).siblings('input').attr('value', url);
                var img = $(el).siblings('img').attr('src', url);
            } );
            finder.on( 'file:choose:resizedImage', function( evt ) {
                var input = $(el).siblings('input').attr('value', evt.data.resizedUrl);
                var img = $(el).siblings('img').attr('src', evt.data.resizedUrl);
            } );
        }
    } );
}
$("button{$this->getElementClassSelector()}").on('click', function() {
    selectFileWithCKFinder(this);
});
EOT;
        return parent::render();
    }
}